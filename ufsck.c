/*
 * ufsck: fsck for ufs
 * main(): Copyright (c) 2015, 2016 Takuo Watanabe
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <error.h>

#include "defs.h"
#include "util.h"

//読みこんだスーパーブロック
superblock   *sb;
//inodeシミュレーション用
bitmapblock  *simbitmap;
uint8_t      *visitedinode;

//inodeのシミュレーションに必要なメモリの確保
void finsim() {
  if(simbitmap!=NULL) free(simbitmap);
  if(visitedinode!=NULL) free(visitedinode);
}
void initsim() {
  simbitmap = calloc(iceil(sb->data.size, BSIZE*8), sizeof(bitmapblock));
  visitedinode = calloc(sb->data.ninodes, 1);
  if(simbitmap==NULL || visitedinode==NULL) {
    finsim();
    exit(EXIT_FAILURE);
  }
}

//スーパーノード一貫性をチェックする
int checksuper(img_t img) {
/* チェック内容
    .                 logstart >= superoff + 1
    .         min     |       inodestart >= logstart + nlog
    .                 |       |       bmapstart >= inodestart + ceil(ninodes/8)
    . 0       1       |       |       |       size - nblocks >= bmapstart + ceil(size/BSIZE/8)
    . v       v       v       v       v       v
    . |boot   |super  |log    |inodes |bitmap |data   |
    .                 ^       ^       ^
    .         max     |       |       bmapstart <= size - nblocks - ceil(size/BSIZE/8)
    .                 |       inodestart <= bmapstart - ceil(ninodes/8)
    .                 logstart <= inodestart - nlog
*/
  sb = (superblock*)getblock(img, 1);
  //サイズから分かる各ブロックの最低最高オフセット
  const int
    binodes       = iceil(sb->data.ninodes, 8),
    bbitmap       = iceil(sb->data.size, BSIZE*8),
    superoff      = 1,
    minlogoff     = superoff            + 1,
    maxlogoff     = sb->data.inodestart - sb->data.nlog,
    mininodeoff   = sb->data.logstart   + sb->data.nlog,
    maxinodeoff   = sb->data.bmapstart  - binodes,
    minbitmapoff  = sb->data.inodestart + binodes,
    maxbitmapoff  = sb->data.size - sb->data.nblocks - bbitmap,
    mindataoff    = sb->data.bmapstart  + bbitmap,
    minsize       = 2+sb->data.nlog+binodes+bbitmap+sb->data.nblocks;
  printf(
      "offset     required <=  actual  <= required\n"
      "logoff:    %08x <= %08x <= %08x\n"
      "inodeoff:  %08x <= %08x <= %08x\n"
      "bitmapoff: %08x <= %08x <= %08x\n"
      "dataoff:   %08x <= %08x\n",
      minlogoff,    sb->data.logstart,    maxlogoff,
      mininodeoff,  sb->data.inodestart,  maxinodeoff,
      minbitmapoff, sb->data.bmapstart,   maxbitmapoff,
      mindataoff,   sb->data.size - sb->data.nblocks);
  //各ブロックのオフセットが[最低,最高]オフセットより[高い,低い]ことをかくにん
  if(minlogoff    > sb->data.logstart   || sb->data.logstart   > maxlogoff    ||
     mininodeoff  > sb->data.inodestart || sb->data.inodestart > maxinodeoff  ||
     minbitmapoff > sb->data.bmapstart  || sb->data.bmapstart  > maxbitmapoff ||
     mindataoff   > sb->data.size - sb->data.nblocks ) {
    printf("error: invalid block offset.\n");
    return 1;
  }
  //サイズがだいたい合っている事をかくにん
  if(minsize > sb->data.size || sb->data.size > minsize+2) {
    printf(
        "error: invalid data size.\n"
        "supposed size=%d +2/-0, actual=%d.\n", minsize, sb->data.size);
    return 1;
  }
  //よかった
  return 0;
}

//datablockのアドレスが正しいかチェック
int checkdbrange(uint32_t dbaddr) {
  const uint32_t
      dbstart     = sb->data.size - sb->data.nblocks, //data block start
      dbend       = sb->data.size;                    //data block end
  return dbaddr<dbstart || dbend<=dbaddr; //範囲内で0
}

//type==1かつ . のinumが同じinodeを探す．(どうせinum==1)
int searchroot(img_t img) {
  //inodeを舐める
  for (int i = 0; i < sb->data.ninodes; i++) {
    inodeblock *ib = (inodeblock*)getblock(img, i+sb->data.inodestart);
    //dinodeを舐める
    const int Diperib = BSIZE/sizeof(dinode);
    for (int j = 0; j < Diperib; j++) {
      int currentinum = i*Diperib + j;
      dinode *targetdi = &ib->data.d[j];
      if(targetdi->type != T_DIR) continue;  //ディレクトリを抽出
      //参照先を取得
      dblist list; int listlen; int dummy;
      if(getdblist(img, targetdi, list, &listlen, checkdbrange, &dummy)) {
        printf("error: cannot continue to search root node.\n");
        return -1;
      }
      //全ての参照先を見る
      const int Entperdb = BSIZE/sizeof(dirent);
      for (int k = 0; k < listlen; k++)
      for (int l = 0; l < Entperdb; l++) {
        if(targetdi->size <= (Entperdb*k + l)*sizeof(dirent)) break;
        dirent *trgdir = &list[k]->data.dir[l];
        //.が指すinumが同じならroot
        if(strcmp(trgdir->name, ".")) continue;
        if(trgdir->inum != currentinum) continue;
        return currentinum;
      }
    }
  }
  return -1;    //見つからなかった
}

//datablockに対応するbitmapをチェック&セット
int checksetbitmap(datablock *db, int errinum) {
    int gdbbn = getdbblocknum(sb, db);
    if(gdbbn==0) {
      printf("error: referencing unused reference.\n" "inum=%08x\n", errinum);
      return 1;
    }
    if(getbitmapelem(simbitmap, gdbbn)) {
      printf(
          "error: duplicated referencing data block.\n"
          "inum=%08x\n", errinum);
      return 1;
    }
    setbitmapelem(simbitmap, gdbbn, 1);
    return 0;
}

//rootノードから辿れるinodeについて実際にinodeを作ってみる
//ついでにbitmapもチェック&セット
//inodeの値の整合性を見ながら辿っていく
//今まで見つかったエラーの数を返す
int visitinode(img_t img, int inum, int inumparent, uint8_t *isdir) {
  const int
    dpd         = BSIZE/sizeof(dirent),             //dirent per data block
    inumstart   = 0,
    inumend     = inumstart + sb->data.ninodes;
  const uint8_t //フラグ(立ってるとダメ)
    ExistsCurrent   = 0x01, // . あるフラグ
    ExistsParent    = 0x02; // .. あるフラグ
  uint8_t flag = ExistsCurrent | ExistsParent;
  dinode *di = getdinode(img, inum, sb);
  int errornum = 0;
  *isdir = 0;
  switch (di->type) {
    case T_DIR: {
      *isdir = !0;
      //既にvisit済みかチェック
      if(visitedinode[inum]) {
        //エラー
        printf("error: duplicated inode reference.\n" "inum=%08x\n", inum);
        return 1;
      }
      visitedinode[inum] = !0;
      //残り子ディレクトリの数
      //note:そんなに子が居なくても/のsize=512だったので上限程度にしかならなそう
      int dirnum = di->size / sizeof(dirent);
      //参照先を取得
      dblist list; int listlen;
      int idbbn;
      if(getdblist(img, di, list, &listlen, checkdbrange, &idbbn)) {
        //間接参照があればbitmapセット
        if(idbbn>0) setbitmapelem(simbitmap, idbbn, 1);
        printf("inum=%08x\n", inum);
        return 1;
      }

      //参照先inodeを調べる
      int childdirnum = 0;
      for (int i = 0; i < listlen; i++, dirnum-=dpd) {

        //loop回避のため既に見たdatablockを参照していないか調べる
        if(checksetbitmap(list[i], inum)) {
          errornum++;
          continue;
        }

        for (int j = 0; j < ((dirnum>=dpd) ? dpd : dirnum); j++) {
          dirent *target = &list[i]->data.dir[j];
          int inumnext = target->inum;
          char *nextname = target->name;

          if(inumnext<inumstart || inumend<=inumnext) {
            printf(
                "error: referencing child inum is out of range.\n"
                "inum=%08x, addrs[*][%02x]=%08x\n", inum, j, inumnext);
            errornum++;
            continue;
          }
          if(inumnext==0) break;       //終端  TODO:終端の後ろに非ゼロが無いか?

          // . と .. の参照先チェック
          if(!strcmp(nextname, ".")) {
            if(~flag & ExistsCurrent) {
              printf(
                "error: duplicated \".\" reference.\n"
                "inum=%08x, referencing=%08x\n", inum, inumnext);
              errornum++;
            }
            flag &= ~ExistsCurrent;
            if(inumnext==inum) continue;  //問題なし，次のノードへ

            //自分を指していない.はおかしい
            printf(
              "error: invalid \".\" reference.\n"
              "inum=%08x, referencing=%08x\n", inum, inumnext);
            errornum++;
            continue;
          } else if(!strcmp(nextname, "..")) {
            if(~flag & ExistsParent) {
              printf(
                "error: duplicated \"..\" reference.\n"
                "inum=%08x, referencing=%08x, actual parent=%08x\n",
                inum, inumnext, inumparent);
              errornum++;
            }
            flag &= ~ExistsParent;
            if(inumnext==inumparent) continue;  //問題無し，次のノードへ

            //親を指していない..はおかしい
            printf(
              "error: invalid \"..\" reference.\n"
              "inum=%08x, referencing=%08x, actual parent=%08x\n",
              inum, inumnext, inumparent);
            errornum++;
            continue;
          }
          //参照先は.や..ではない
          //子inodeを再帰的に辿る
          printf("visiting: inum=%08x, name=%s\n", inumnext, nextname);
          uint8_t childisdir;
          errornum += visitinode(img, inumnext, inum, &childisdir);
          childdirnum += childisdir ? 1 : 0;
        }
      }

      //nlinkは合っているか
      if(di->nlink!=childdirnum+1) {
        printf(
          "error: invalid nlink.\n"
          "supposed=%d, actual=%d, in inum=%08x\n",
          childdirnum, di->nlink, inum);
        errornum++;
      }

      // . と .. が有ったか
      if(flag) {
        printf(
          "error: either or both of . and .. don't exist.\n"
          "inum=%08x, flag=%08x\n", inum, flag);
        errornum++;
      }
    } break;
    case T_FILE: {
      //TODO:nlinkをチェックしていない
      //既にvisit済みかチェック
      if(visitedinode[inum]) {
        //linkが張れるためエラーではない
        return 0;
      }
      visitedinode[inum] = !0;
      //参照先を取得
      dblist list; int listlen;
      int idbbn;
      if(getdblist(img, di, list, &listlen, checkdbrange, &idbbn)) {
        printf("inum=%08x\n", inum);
        return 1;
      }
      //間接参照があればbitmapセット
      if(idbbn>0) setbitmapelem(simbitmap, idbbn, 1);
      //listエラーチェック済み
      for(int i=0; i<listlen; i++) {
        //T_FILEでもvisitedinodeのおかげでこのinodeには1回しか入らないため，
        //単純なbitmapの重複検知ができる
        if(checksetbitmap(list[i], inum)) { errornum++; continue; }
      }
    } break;
    case T_DEV:
      if(di->major==0) {
        printf("error: invalid device major number.\n" "inum=%08x", inum);
      }
      break;
    default:
      printf("error: invalid inode type inum=%08x, type=%d\n", inum, di->type);
      break;
  }
  return errornum;
}

//rootのinodeから辿れるinodeを検査しながら追加していく
int simulateinodebitmap(img_t img) {
  int inum = searchroot(img);
  if(inum<0) {
    printf("error: no inode root found.\n");
    return 1;
  }
  uint8_t dummy;
  //rootnodeの親inodeはrootnodeのinum
  //エラー数を返す
  return visitinode(img, inum, inum, &dummy);
}

//inodeシミュレートで作ったbitmapと比較
int checkbitmapblock(img_t img) {
  const int dbblocknum = sb->data.size - sb->data.nblocks;
  //実際のbitmap
  bitmapblock *bmb = (bitmapblock*)getblock(img, sb->data.bmapstart);
  int errornum = 0;

  for(int i=dbblocknum; i<sb->data.size; i++) {
    //0 or 1
    uint8_t
      actual  = getbitmapelem(bmb, i),
      suppose = getbitmapelem(simbitmap, i);
    if(actual == suppose)
      continue;
    //異常
    printf(
        "error: bitmap error detected.\n"
        "  bitmap[%04x]: supposed=%d, actual=%d\n", i, suppose, actual);
    errornum++;
  }
  return errornum;
}

int main(int argc, char *argv[]) {
  char *progname = argv[0];
  if (argc < 2) {
    perror("usage: ufsck img_file\n");
    return EXIT_FAILURE;
  }
  char *img_file = argv[1];

  int img_fd = open(img_file, O_RDWR);
  if (img_fd < 0) {
    perror(img_file);
    return EXIT_FAILURE;
  }

  struct stat img_sbuf;
  if (fstat(img_fd, &img_sbuf) < 0) {
    perror(img_file);
    close(img_fd);
    return EXIT_FAILURE;
  }
  size_t img_size = (size_t)img_sbuf.st_size;

  img_t img = (img_t)mmap(NULL, img_size, PROT_READ | PROT_WRITE,
      MAP_SHARED, img_fd, 0);
  if (img == MAP_FAILED) {
    perror(img_file);
    close(img_fd);
    return EXIT_FAILURE;
  }

  if(checksuper(img)) {
    printf("Detected superblock erorrs while checking supernode.\n");
    return 1;
  }

  int errornum = 0;
  initsim();

  do {
    errornum = simulateinodebitmap(img);
    if(errornum) {
      printf("Detected %d erorrs while visiting inode.\n", errornum);
      break;
    }

    errornum = checkbitmapblock(img);
    if(errornum) {
      printf("Detected %d erorrs while bitmap checking.\n", errornum);
      break;
    }
  } while(0);

  finsim();
  munmap(img, img_size);
  close(img_fd);
  if(!errornum) printf("error: no error detected.\n");

  return errornum;
}
