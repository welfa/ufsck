#ifndef DEFS
#define DEFS

#include <stdint.h>
#define iceil(x, y) (((x)-1) / (y) + 1)
#define max(x, y)   ((x)>(y) ? x : y)
#define min(x, y)   ((x)>(y) ? y : x)

#define BSIZE   512
#define T_DIR   1
#define T_FILE  2
#define T_DEV   3

typedef uint8_t (*img_t)[BSIZE];

#define NDIRECT 12
typedef struct  __attribute__((__packed__)) {
    uint16_t    type;
    uint16_t    major;
    uint16_t    minor;
    uint16_t    nlink;
    uint32_t    size;
    uint32_t    addrs[NDIRECT+1];
} dinode;

#define DIRSIZ 14
typedef struct  __attribute__((__packed__)) {
    uint16_t    inum;
    char        name[DIRSIZ];
} dirent;

typedef union   {
    struct {
        uint32_t    size;
        uint32_t    nblocks;
        uint32_t    ninodes;
        uint32_t    nlog;
        uint32_t    logstart;
        uint32_t    inodestart;
        uint32_t    bmapstart;
    } data;
    uint8_t     raw[BSIZE];
} superblock;

typedef union   {
    struct {
        dinode      d[BSIZE/sizeof(dinode)];
    } data;
    uint8_t     raw[BSIZE];
} inodeblock;

typedef union   {
    uint8_t     raw[BSIZE];
} bitmapblock;

typedef union   {
    struct {
        dirent      dir[BSIZE/sizeof(dirent)];
    } data;
    uint32_t    indirectaddr[BSIZE/4];
    uint8_t     raw[BSIZE];
} datablock;

#endif /* end of include guard */
