#include <stdio.h>
#include "util.h"

//ブロックナンバからblockを引く
void *getblock(img_t img, int blocknum) { return img[blocknum]; }
//datablockのblock番号をとる
int getdbblocknum(superblock *sb, datablock *db) { return db-(datablock*)sb+1; }

//direntのinumからinodeblockを引く
dinode *getdinode(img_t img, int inum, superblock *sb) {
  const int dpi = BSIZE/sizeof(dinode);   //dinode per inode
  const int inumblock = inum / dpi + sb->data.inodestart;
  inodeblock *ib = (inodeblock*)getblock(img,inumblock);
  return &ib->data.d[inum%dpi];
}

//ブロックナンバでbitmapblockの要素にアクセス
int getbitmapelem(bitmapblock *bmb, int blocknum) {
  uint32_t
    a = blocknum / 8,   //byte -> bit
    b = a / BSIZE,      //bmb index
    c = a % BSIZE,      //bmb element index
    d = blocknum % 8;   //bit index
  return (bmb[b].raw[c] & (1<<d)) ? 1 : 0;
}
//ブロックナンバでbitmapblockの要素にアクセス，書き込む
int setbitmapelem(bitmapblock *bmb, int blocknum, int value) {
  uint32_t
    a = blocknum / 8,   //byte -> bit
    b = a / BSIZE,      //bmb index
    c = a % BSIZE,      //bmb element index
    d = blocknum % 8;   //bit index
  uint8_t mask = 1<<d;
  if(value) bmb[b].raw[c] |=  mask;
  else      bmb[b].raw[c] &= ~mask;
}

//dinodeを見て参照しているdatablockのリストを返す
int getdblist(
    img_t img, dinode *di,
    dblist list, int *length,
    fn_checkdbrange check,
    int *indirectdatablock) {
  *length = iceil(di->size, BSIZE);         //listに入れるdatablockの数
  *indirectdatablock = di->addrs[NDIRECT];  //*length<NDIRECTでも0になるだけ
  for (int i = 0; i < *length; i++) {
    uint32_t dbnum = i<NDIRECT ?
      di->addrs[i] :
      ((datablock*)getblock(img, *indirectdatablock))->indirectaddr[i-NDIRECT];
    if(dbnum==0) { printf("error: invalid size.\n"); return 1; }
    if(check(dbnum)) {
      printf(
          "error: %s referencing datablock address in dinode is out of range.\n"
          "referencing datablock block number = %8x\n",
          i<NDIRECT ? "directly" : "indirectly", dbnum);
      return 1;
    }
    list[i] = (datablock*)getblock(img, dbnum);
  }
  return 0;
}
