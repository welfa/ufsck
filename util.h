#ifndef UTIL
#define UTIL

#include <stdint.h>
#include "defs.h"

void *getblock(img_t img, int blocknum);
int getdbblocknum(superblock *sb, datablock *db);
dinode *getdinode(img_t img, int inum, superblock *sb);
int getbitmapelem(bitmapblock *bmb, int blocknum);
int setbitmapelem(bitmapblock *bmb, int blocknum, int value);
typedef datablock *dblist[NDIRECT+BSIZE/4];
typedef int (*fn_checkdbrange)(uint32_t); //異常でnon0を返す
int getdblist(
    img_t img, dinode *di,
    dblist list, int *length,
    fn_checkdbrange check,
    int *indirectdatablock);

#endif /* end of include guard */
